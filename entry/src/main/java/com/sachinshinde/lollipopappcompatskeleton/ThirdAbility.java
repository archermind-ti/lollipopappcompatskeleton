package com.sachinshinde.lollipopappcompatskeleton;

import com.sachinshinde.lollipopappcompatskeleton.slice.ThirdAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class ThirdAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ThirdAbilitySlice.class.getName());
    }
}
