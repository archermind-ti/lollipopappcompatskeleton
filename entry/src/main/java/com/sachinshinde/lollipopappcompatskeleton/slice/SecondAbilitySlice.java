package com.sachinshinde.lollipopappcompatskeleton.slice;

import com.sachinshinde.lollipopappcompatskeleton.MainAbility;
import com.sachinshinde.lollipopappcompatskeleton.ResourceTable;
import com.sachinshinde.lollipopappcompatskeleton.SecondAbility;
import com.sachinshinde.lollipopappcompatskeleton.ThirdAbility;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.Point;
import ohos.agp.window.dialog.PopupDialog;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.DisplayManager;
import ohos.agp.window.service.Window;
import ohos.agp.window.service.WindowManager;


public class SecondAbilitySlice extends AbilitySlice {
    private float screenWidth;
    private boolean drawerOpen = false;
    private Component popup_dialog;
    private float background_alpha;
    private Component drawerBg;
    private boolean running = false;
    private DependentLayout parentLayout;
    private AnimatorProperty animatorProperty;
    private DirectionalLayout dialog, textLayoutAll;
    private Component root;
    PopupDialog toastDialog;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_second);
        Window window = getWindow();
        window.setInputPanelDisplayType(WindowManager.LayoutConfig.INPUT_ADJUST_PAN);
        root = findComponentById(ResourceTable.Layout_ability_second);
        Point point = new Point();
        initColor();
        DisplayManager.getInstance().getDefaultDisplay(this).get().getSize(point);
        screenWidth = point.getPointX();
        parentLayout = (DependentLayout) findComponentById(ResourceTable.Id_parent_layout_two);
        dialog = (DirectionalLayout) findComponentById(ResourceTable.Id_dialog2);
        textLayoutAll = (DirectionalLayout) findComponentById(ResourceTable.Id_text_layout_all);
        initView();
        initClick();
    }

    private void initColor() {
        Text text = (Text) findComponentById(ResourceTable.Id_txt_section_2);
        text.setTextColor(new Color(getColor(ResourceTable.Color_theme_primary_dark)));
    }

    private void initClick() {
        findComponentById(ResourceTable.Id_show_toast).setClickedListener(component -> {
            DirectionalLayout toastlayout = (DirectionalLayout) LayoutScatter.getInstance(getContext()).parse(
                    ResourceTable.Layout_layout_toast, null, false);
            toastlayout.setClickedListener(component1 -> {
                toastDialog.remove();
                LayoutScatter layoutScatter = LayoutScatter.getInstance(getContext());
                DirectionalLayout layout = (DirectionalLayout) layoutScatter.parse(ResourceTable.Layout_layout_toast2, null, false);
                new ToastDialog(getContext())
                        .setSize(DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT)
                        .setComponent(layout)
                        .show();
            });
            toastDialog = new PopupDialog(getContext(), root);
            toastDialog.setOffset(0, -100);
            toastDialog.setSize(DirectionalLayout.LayoutConfig.MATCH_PARENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT);
            toastDialog.setCustomComponent(toastlayout);
            toastDialog.setAlignment(LayoutAlignment.BOTTOM);
            toastDialog.show();
            getContext().getUITaskDispatcher().delayDispatch(() -> {
                if (toastDialog != null && toastDialog.isShowing()) {
                    toastDialog.remove();
                }
            }, 1500);
        });
        findComponentById(ResourceTable.Id_show_animator_two).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (!running) {
                    otherClickable(false);
                    circleLoading();
                }
                running = true;
            }
        });
    }

    private void otherClickable(boolean b) {
        DependentLayout root = (DependentLayout) findComponentById(ResourceTable.Id_root);
        childClickable(b, root);
    }

    private void childClickable(boolean b, ComponentContainer componentContainer) {
        if (componentContainer.getChildCount() > 0)
            for (int i = 0; i < componentContainer.getChildCount(); i++) {
                Component componentAt = componentContainer.getComponentAt(i);
                componentAt.setClickable(b);
                if (componentAt instanceof PageSlider)
                    ((PageSlider) componentAt).setSlidingPossible(b);
                if (componentAt instanceof ComponentContainer)
                    childClickable(b, (ComponentContainer) componentAt);
            }
    }

    private void circleLoading() {
        ShapeElement element = new ShapeElement();
        element.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor("#55000000")));
        parentLayout.setForeground(element);
        textLayoutAll.setForeground(element);
        dialog.setVisibility(Component.VISIBLE);
        Component animatorComponent = findComponentById(ResourceTable.Id_animatorComponent);
        final int[] count = {0};
        animatorProperty = animatorComponent.createAnimatorProperty()
                .scaleXFrom(0).scaleX(1)
                .scaleYFrom(0).scaleY(1)
                .alphaFrom(1).alpha(0)
                .setDuration(500)
                .setCurveType(Animator.CurveType.LINEAR)
                .setLoopedCount(-1)
                .setLoopedListener(animator -> {
                    ComponentContainer componentParent = (ComponentContainer) animatorComponent.getComponentParent();
                    int parentWidth = componentParent.getWidth();
                    int i = (parentWidth - animatorComponent.getMarginLeft() * 2 - animatorComponent.getWidth()) / 2;
                    if (count[0] < 2) {
                        animatorComponent.setContentPositionX(animatorComponent.getContentPositionX() + i);
                        count[0]++;
                    } else {
                        animatorComponent.setContentPositionX(animatorComponent.getMarginLeft());
                        count[0] = 0;
                    }
                });
        animatorProperty.start();
    }

    private void initView() {
        drawerBg = findComponentById(ResourceTable.Id_drawerBg);
        popup_dialog = findComponentById(ResourceTable.Id_popup_dialog);
        popup_dialog.setTranslationX(-screenWidth);
//        popup_dialog.setTranslationX(-popup_dialog.getWidth());
        background_alpha = popup_dialog.getAlpha();
        findComponentById(ResourceTable.Id_title_click).setClickedListener(component -> {
            startAnimation();
            ShapeElement element = new ShapeElement();
            element.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor("#55000000")));
            parentLayout.setForeground(element);
        });
        drawerBg.setClickedListener(component -> {
            if (!drawerOpen) {
                return;
            }
            startAnimation();
            parentLayout.setForeground(null);
        });
        popup_dialog.findComponentById(ResourceTable.Id_section1).setClickedListener(component -> {
            Intent intent = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName(getBundleName())
                    .withAbilityName(MainAbility.class)
                    .build();
            intent.setOperation(operation);
            startAbility(intent);
            terminateAbility();
        });
        popup_dialog.findComponentById(ResourceTable.Id_section2).setClickedListener(component -> {
            Intent intent = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName(getBundleName())
                    .withAbilityName(SecondAbility.class)
                    .build();
            intent.setOperation(operation);
            startAbility(intent);
            terminateAbility();
        });
        popup_dialog.findComponentById(ResourceTable.Id_section3).setClickedListener(component -> {
            Intent intent = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName(getBundleName())
                    .withAbilityName(ThirdAbility.class)
                    .build();
            intent.setOperation(operation);
            startAbility(intent);
            terminateAbility();
        });
    }

    /**
     * 抽屉开关动画
     */
    private void startAnimation() {
        AnimatorValue anim = new AnimatorValue();
        anim.setDuration(200);
        anim.setCurveType(Animator.CurveType.LINEAR);
        anim.setValueUpdateListener((animatorValue, v) -> {
            float tranX = drawerOpen ? (-screenWidth * v) : (screenWidth * v - screenWidth);
            drawerBg.setAlpha((int) (drawerOpen ? (background_alpha - background_alpha * v) : (background_alpha * v)));
            popup_dialog.setTranslationX(tranX);
        });
        anim.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
                running = true;

                if (!drawerOpen) {
                    drawerBg.setVisibility(Component.VISIBLE);
                }
            }

            @Override
            public void onStop(Animator animator) {
                running = false;
                drawerOpen = !drawerOpen;
                if (!drawerOpen) {
                    drawerBg.setVisibility(Component.HIDE);
                }
            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {

            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        anim.start();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    protected void onBackPressed() {
        if (dialog != null && dialog.getVisibility() == Component.VISIBLE) {
            otherClickable(true);
            dialog.setVisibility(Component.HIDE);
            animatorProperty.reset();
            parentLayout.setForeground(null);
            textLayoutAll.setForeground(null);
            running = false;
        } else if (drawerOpen) {
            startAnimation();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
