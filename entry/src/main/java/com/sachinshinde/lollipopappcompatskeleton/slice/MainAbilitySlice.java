package com.sachinshinde.lollipopappcompatskeleton.slice;

import com.sachinshinde.lollipopappcompatskeleton.MainAbility;
import com.sachinshinde.lollipopappcompatskeleton.ResourceTable;
import com.sachinshinde.lollipopappcompatskeleton.SecondAbility;
import com.sachinshinde.lollipopappcompatskeleton.ThirdAbility;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.Point;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.PopupDialog;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.DisplayManager;
import ohos.agp.window.service.Window;
import ohos.agp.window.service.WindowManager;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

import java.util.ArrayList;
import java.util.List;


public class MainAbilitySlice extends AbilitySlice {

    private float screenWidth;
    private boolean drawerOpen = false;
    private Component popup_dialog;
    private float background_alpha;
    private Component drawerBg;
    private boolean running = false;
    private PageSlider pageSlider;
    private TabList tabList;
    private DirectionalLayout dialog;
    private DependentLayout parentLayout;
    private Text mText;
    private AnimatorProperty animatorProperty;
    private List<String> tablists = new ArrayList<>();
    private LayoutScatter layoutScatter;
    private DirectionalLayout layoutOne, layoutTwo, layoutThree;
    Text mTxtSection1, mTxtSection2, mTxtSection3;
    PopupDialog toastDialog;
    private Component root;
    private boolean isSelectable = true;
    private int selectedTabIndex = 0;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        Window window = getWindow();
        window.setInputPanelDisplayType(WindowManager.LayoutConfig.INPUT_ADJUST_PAN);
        root = findComponentById(ResourceTable.Id_root);
        initText();
        initColor();
        Point point = new Point();
        DisplayManager.getInstance().getDefaultDisplay(this).get().getSize(point);
//        LayoutScatter layoutScatter = LayoutScatter.getInstance(getContext());
//        DependentLayout dialogLayout = (DependentLayout) layoutScatter.parse(ResourceTable.Layout_animator_dialog,null,false);
        dialog = (DirectionalLayout) findComponentById(ResourceTable.Id_dialog1);
        screenWidth = point.getPointX();
        parentLayout = (DependentLayout) findComponentById(ResourceTable.Id_parent_layout);
        {
            tablists.add("POSITION AT 1");
            tablists.add("POSITION AT 2");
            tablists.add("POSITION AT 3");
        }
        pageSlider = (PageSlider) findComponentById(ResourceTable.Id_pageSlider);
        tabList = (TabList) findComponentById(ResourceTable.Id_tab_list);
        for (String tablist : tablists) {
            TabList.Tab tab = tabList.new Tab(getContext());
            tab.setText(tablist);
            tab.setTextSize(10, Text.TextSizeType.FP);
            tabList.addTab(tab);
            tabList.setOrientation(Component.HORIZONTAL);
            tabList.selectTabAt(0);
            tabList.addTabSelectedListener(new TabList.TabSelectedListener() {
                @Override
                public void onSelected(TabList.Tab tab) {
                    //当某个Tab从未选中状态变为选中状态时的回调
                    if (isSelectable)
                        pageSlider.setCurrentPage(tab.getPosition());
                    else
                        tabList.selectTabAt(selectedTabIndex);
                }

                @Override
                public void onUnselected(TabList.Tab tab) {

                }

                @Override
                public void onReselected(TabList.Tab tab) {

                }
            });
        }
        layoutScatter = LayoutScatter.getInstance(getContext());
        layoutOne = (DirectionalLayout) layoutScatter.parse(ResourceTable.Layout_tab_show, null, false);
        layoutTwo = (DirectionalLayout) layoutScatter.parse(ResourceTable.Layout_table_show_two, null, false);
        layoutThree = (DirectionalLayout) layoutScatter.parse(ResourceTable.Layout_table_show_three, null, false);
        createAnimator(layoutOne, layoutTwo, layoutThree);
        createToastDialog(layoutOne, layoutTwo, layoutThree);
        ArrayList<Component> pageView = new ArrayList<>();
        pageView.add(layoutOne);
        pageView.add(layoutTwo);
        pageView.add(layoutThree);

        pageSlider.setProvider(new PageSliderProvider() {
            @Override
            public int getCount() {
                return pageView.size();
            }

            @Override
            public Object createPageInContainer(ComponentContainer componentContainer, int i) {
                componentContainer.addComponent(pageView.get(i));
                return pageView.get(i);
            }

            @Override
            public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
                componentContainer.removeComponent(pageView.get(i));
            }

            @Override
            public boolean isPageMatchToObject(Component component, Object o) {
                return component == o;
            }
        });
        pageSlider.setCurrentPage(0);
        pageSlider.addPageChangedListener(new PageSlider.PageChangedListener() {
            @Override
            public void onPageSliding(int i, float v, int i1) {

            }

            @Override
            public void onPageSlideStateChanged(int i) {

            }

            @Override
            public void onPageChosen(int i) {
                tabList.selectTabAt(i);
            }
        });
        initView();
    }

    private void initColor() {
        Text text = (Text) findComponentById(ResourceTable.Id_txt_section1);
        text.setTextColor(new Color(getColor(ResourceTable.Color_theme_primary_dark)));
    }

    private void initText() {
        mTxtSection1 = (Text) findComponentById(ResourceTable.Id_txt_section1);
        mTxtSection2 = (Text) findComponentById(ResourceTable.Id_txt_section2);
        mTxtSection3 = (Text) findComponentById(ResourceTable.Id_txt_section3);
    }

    private void createAnimator(DirectionalLayout layout1, DirectionalLayout layout2, DirectionalLayout layout3) {

        layout1.findComponentById(ResourceTable.Id_show_animator).setClickedListener(component -> {
            if (!running) {
                otherClickable(false);
                circleLoading();
            }
            running = true;
        });
        layout2.findComponentById(ResourceTable.Id_show_animator_two).setClickedListener(component -> {
            if (!running) {
                otherClickable(false);
                circleLoading();
            }
            running = true;
        });
        layout3.findComponentById(ResourceTable.Id_show_animator_three).setClickedListener(component -> {
            if (!running) {
                otherClickable(false);
                circleLoading();
            }
            running = true;
        });

    }

    private void otherClickable(boolean b) {
        selectedTabIndex = tabList.getSelectedTabIndex();
        isSelectable = b;
        DependentLayout root = (DependentLayout) findComponentById(ResourceTable.Id_root);
        childClickable(b, root);
    }

    private void childClickable(boolean b, ComponentContainer componentContainer) {
        if (componentContainer.getChildCount() > 0)
            for (int i = 0; i < componentContainer.getChildCount(); i++) {
                Component componentAt = componentContainer.getComponentAt(i);
                componentAt.setClickable(b);
                if (componentAt instanceof PageSlider)
                    ((PageSlider) componentAt).setSlidingPossible(b);
                if (componentAt instanceof ComponentContainer)
                    childClickable(b, (ComponentContainer) componentAt);
            }
    }

    private void circleLoading() {
        ShapeElement element = new ShapeElement();
        element.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor("#55000000")));
        parentLayout.setForeground(element);
        dialog.setVisibility(Component.VISIBLE);
        Component animatorComponent = findComponentById(ResourceTable.Id_animatorComponent);
        final int[] count = {0};
        animatorProperty = animatorComponent.createAnimatorProperty()
                .scaleXFrom(0).scaleX(1)
                .scaleYFrom(0).scaleY(1)
                .alphaFrom(1).alpha(0)
                .setDuration(500)
                .setCurveType(Animator.CurveType.LINEAR)
                .setLoopedCount(-1)
                .setLoopedListener(animator -> {
                    ComponentContainer componentParent = (ComponentContainer) animatorComponent.getComponentParent();
                    int parentWidth = componentParent.getWidth();
                    int i = (parentWidth - animatorComponent.getMarginLeft() * 2 - animatorComponent.getWidth()) / 2;
                    if (count[0] < 2) {
                        animatorComponent.setContentPositionX(animatorComponent.getContentPositionX() + i);
                        count[0]++;
                    } else {
                        animatorComponent.setContentPositionX(animatorComponent.getMarginLeft());
                        count[0] = 0;
                    }
                });
        animatorProperty.start();
    }

    private void createToastDialog(DirectionalLayout layout1, DirectionalLayout layout2, DirectionalLayout layout3) {

        layout1.findComponentById(ResourceTable.Id_show_toast).setClickedListener(component -> {
            DirectionalLayout toastlayout = (DirectionalLayout) LayoutScatter.getInstance(getContext()).parse(
                    ResourceTable.Layout_layout_toast, null, false);
            toastlayout.setClickedListener(component1 -> {
                toastDialog.remove();
                component.setClickable(true);
                LayoutScatter layoutScatter = LayoutScatter.getInstance(getContext());
                DirectionalLayout layout = (DirectionalLayout) layoutScatter.parse(ResourceTable.Layout_layout_toast2, null, false);
                new ToastDialog(getContext())
                        .setSize(DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT)
                        .setComponent(layout)
                        .show();
            });
            toastDialog = new PopupDialog(getContext(), root);
            toastDialog.setSize(DirectionalLayout.LayoutConfig.MATCH_PARENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT);
            toastDialog.setCustomComponent(toastlayout);
            toastDialog.setAlignment(LayoutAlignment.BOTTOM);
            toastDialog.show();
            component.setClickable(!toastDialog.isShowing());
            getContext().getUITaskDispatcher().delayDispatch(() -> {
                if (toastDialog != null && toastDialog.isShowing()) {
                    toastDialog.remove();
                    component.setClickable(true);
                }
            }, 1500);
        });

        layout2.findComponentById(ResourceTable.Id_show_toast_two).setClickedListener(component -> {
            DirectionalLayout toastlayout = (DirectionalLayout) LayoutScatter.getInstance(getContext()).parse(
                    ResourceTable.Layout_layout_toast, null, false);
            toastlayout.setClickedListener(component1 -> {
                toastDialog.remove();
                component.setClickable(true);
                LayoutScatter layoutScatter = LayoutScatter.getInstance(getContext());
                DirectionalLayout layout = (DirectionalLayout) layoutScatter.parse(ResourceTable.Layout_layout_toast2, null, false);
                new ToastDialog(getContext())
                        .setSize(DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT)
                        .setComponent(layout)
                        .show();
            });
            toastDialog = new PopupDialog(getContext(), root);
            toastDialog.setOffset(0, -100);
            toastDialog.setSize(DirectionalLayout.LayoutConfig.MATCH_PARENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT);
            toastDialog.setCustomComponent(toastlayout);
            toastDialog.setAlignment(LayoutAlignment.BOTTOM);
            toastDialog.show();
            component.setClickable(!toastDialog.isShowing());
            getContext().getUITaskDispatcher().delayDispatch(() -> {
                if (toastDialog != null && toastDialog.isShowing()) {
                    toastDialog.remove();
                    component.setClickable(true);
                }
            }, 1500);
        });

        layout3.findComponentById(ResourceTable.Id_show_toast_three).setClickedListener(component -> {
            DirectionalLayout toastlayout = (DirectionalLayout) LayoutScatter.getInstance(getContext()).parse(
                    ResourceTable.Layout_layout_toast, null, false);
            toastlayout.setClickedListener(component1 -> {
                toastDialog.remove();
                component.setClickable(true);
                LayoutScatter layoutScatter = LayoutScatter.getInstance(getContext());
                DirectionalLayout layout = (DirectionalLayout) layoutScatter.parse(ResourceTable.Layout_layout_toast2, null, false);
                new ToastDialog(getContext())
                        .setSize(DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT)
                        .setComponent(layout)
                        .show();
            });
            toastDialog = new PopupDialog(getContext(), root);
            toastDialog.setOffset(0, -100);
            toastDialog.setSize(DirectionalLayout.LayoutConfig.MATCH_PARENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT);
            toastDialog.setCustomComponent(toastlayout);
            toastDialog.setAlignment(LayoutAlignment.BOTTOM);
            toastDialog.show();
            component.setClickable(!toastDialog.isShowing());
            getContext().getUITaskDispatcher().delayDispatch(() -> {
                if (toastDialog != null && toastDialog.isShowing()) {
                    toastDialog.remove();
                    component.setClickable(true);
                }
            }, 1500);
        });
    }

    private void initView() {
        drawerBg = findComponentById(ResourceTable.Id_drawerBg);
        popup_dialog = findComponentById(ResourceTable.Id_popup_dialog);
        popup_dialog.setTranslationX(-screenWidth);
//        popup_dialog.setTranslationX(-popup_dialog.getWidth());
        background_alpha = popup_dialog.getAlpha();
        findComponentById(ResourceTable.Id_title_click).setClickedListener(component -> startAnimation());

        drawerBg.setClickedListener(component -> {
            if (!drawerOpen) {
                return;
            }
            startAnimation();
        });
        popup_dialog.findComponentById(ResourceTable.Id_section1).setClickedListener(component -> {
            mTxtSection1.setTextColor(new Color(ResourceTable.Color_theme_primary));
            Intent intent = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName(getBundleName())
                    .withAbilityName(MainAbility.class)
                    .build();
            intent.setOperation(operation);
            startAbility(intent);
            terminateAbility();
        });
        popup_dialog.findComponentById(ResourceTable.Id_section2).setClickedListener(component -> {
            mTxtSection2.setTextColor(new Color(ResourceTable.Color_theme_primary));
            Intent intent = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName(getBundleName())
                    .withAbilityName(SecondAbility.class)
                    .build();
            intent.setOperation(operation);
            startAbility(intent);
            terminateAbility();
        });
        popup_dialog.findComponentById(ResourceTable.Id_section3).setClickedListener(component -> {
            mTxtSection3.setTextColor(new Color(ResourceTable.Color_theme_primary));
            Intent intent = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName(getBundleName())
                    .withAbilityName(ThirdAbility.class)
                    .build();
            intent.setOperation(operation);
            startAbility(intent);
            terminateAbility();
        });
    }

    /**
     * 抽屉开关动画
     */
    private void startAnimation() {
        AnimatorValue anim = new AnimatorValue();
        anim.setDuration(200);
        anim.setCurveType(Animator.CurveType.LINEAR);
        anim.setValueUpdateListener((animatorValue, v) -> {
            float tranX = drawerOpen ? (-screenWidth * v) : (screenWidth * v - screenWidth);
            drawerBg.setAlpha((int) (drawerOpen ? (background_alpha - background_alpha * v) : (background_alpha * v)));
            popup_dialog.setTranslationX(tranX);
        });
        anim.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
                running = true;

                if (!drawerOpen) {
                    drawerBg.setVisibility(Component.VISIBLE);
                }
            }

            @Override
            public void onStop(Animator animator) {
                running = false;
                drawerOpen = !drawerOpen;
                if (!drawerOpen) {
                    drawerBg.setVisibility(Component.HIDE);
                }
            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        anim.start();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onBackPressed() {
        if (dialog != null && dialog.getVisibility() == Component.VISIBLE) {
            otherClickable(true);
            dialog.setVisibility(Component.HIDE);
            animatorProperty.reset();
            parentLayout.setForeground(null);
            running = false;
        } else if (drawerOpen) {
            startAnimation();
        } else {
            super.onBackPressed();
        }
    }
}
