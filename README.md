# Lollipop AppCompat Skeleton

### 项目介绍

一个material设计风格的导航抽屉框架组件。

###  安装教程

无

### 使用说明

- 抽屉开关动画

  ```java
  private void startAnimation() {
      AnimatorValue anim = new AnimatorValue();
      anim.setDuration(200);
      anim.setCurveType(Animator.CurveType.LINEAR);
      anim.setValueUpdateListener((animatorValue, v) -> {
          float tranX = drawerOpen ? (-screenWidth * v) : (screenWidth * v - screenWidth);
          drawerBg.setAlpha((int) (drawerOpen ? (background_alpha - background_alpha * v) : (background_alpha * v)));
          popup_dialog.setTranslationX(tranX);
      });
      anim.setStateChangedListener(new Animator.StateChangedListener() {
          @Override
          public void onStart(Animator animator) {
              running = true;
  
              if (!drawerOpen) {
                  drawerBg.setVisibility(Component.VISIBLE);
              }
          }
  
          @Override
          public void onStop(Animator animator) {
              running = false;
              drawerOpen = !drawerOpen;
              if (!drawerOpen) {
                  drawerBg.setVisibility(Component.HIDE);
              }
          }
  
          @Override
          public void onCancel(Animator animator) {
  
          }
  
          @Override
          public void onEnd(Animator animator) {
          }
  
          @Override
          public void onPause(Animator animator) {
  
          }
  
          @Override
          public void onResume(Animator animator) {
  
          }
      });
      anim.start();
  }
  ```
  
- 返回键收起dialog

  ```java
  @Override
  protected void onBackPressed() {
      if (dialog != null && dialog.getVisibility() == Component.VISIBLE) {
          otherClickable(true);
          dialog.setVisibility(Component.HIDE);
          animatorProperty.reset();
          parentLayout.setForeground(null);
          running = false;
      } else if (drawerOpen) {
          startAnimation();
      } else {
          super.onBackPressed();
      }
  }
  ```
  
- loading动画

  ```java
  private void circleLoading() {
          ShapeElement element = new ShapeElement();
          element.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor("#55000000")));
          parentLayout.setForeground(element);
          dialog.setVisibility(Component.VISIBLE);
          Component animatorComponent = findComponentById(ResourceTable.Id_animatorComponent);
          final int[] count = {0};
          animatorProperty = animatorComponent.createAnimatorProperty()
                  .scaleXFrom(0).scaleX(1)
                  .scaleYFrom(0).scaleY(1)
                  .alphaFrom(1).alpha(0)
                  .setDuration(500)
                  .setCurveType(Animator.CurveType.LINEAR)
                  .setLoopedCount(-1)
                  .setLoopedListener(animator -> {
                      ComponentContainer componentParent = (ComponentContainer) animatorComponent.getComponentParent();
                      int parentWidth = componentParent.getWidth();
                      int i = (parentWidth - animatorComponent.getMarginLeft() * 2 - animatorComponent.getWidth()) / 2;
                      if (count[0] < 2) {
                          animatorComponent.setContentPositionX(animatorComponent.getContentPositionX() + i);
                          count[0]++;
                      } else {
                          animatorComponent.setContentPositionX(animatorComponent.getMarginLeft());
                          count[0] = 0;
                      }
                  });
          animatorProperty.start();
      }
  ```

### 效果演示

![](1.gif)


###  版本迭代

- v1.0.0

## License

Lollipop AppCompat Skeleton在 [Apache 2.0 License](LICENSE)下获得许可

